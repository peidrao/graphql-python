from django.contrib import admin

# Register your models here.
from .models import Author, Book, BookImage


admin.site.register(Author)
admin.site.register(Book)
admin.site.register(BookImage)