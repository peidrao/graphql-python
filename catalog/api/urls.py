from rest_framework.routers import DefaultRouter

from catalog.api.views import AuthorViewSet, BookViewSet

app_name = 'catalog-api'

router = DefaultRouter()

router.register('authors', AuthorViewSet)
router.register('books', BookViewSet)

urlpatterns = router.urls

