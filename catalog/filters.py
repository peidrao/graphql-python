from django_filters import FilterSet, CharFilter 

from .models import Book


class BookFilter(FilterSet):
    title = CharFilter(lookup_expr='icontains')
    author__first_name = CharFilter(lookup_expr='icontains')
    author__last_name = CharFilter(lookup_expr='icontains')

    class Meta:
        model = Book
        fields = ('title', 'author__first_name', 'author__last_name')