from django.db import models


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    
    class Meta:
        ordering = ['first_name', 'last_name']
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'
    
    def __str__(self) -> str:
        return str(self.first_name)

class Book(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.DO_NOTHING, null=True)
    summary = models.TextField(max_length=1000, help_text='Enter a brief description')

    class Meta:
        ordering = ['-id']
        verbose_name = 'Book'
        verbose_name_plural = 'Books'
    
    def __str__(self) -> str:
        return str(self.title)


class BookImage(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    url = models. CharField(max_length=255)

    class Meta:
        verbose_name = 'BookImage'
        verbose_name_plural = 'BookImages'

    def __str__(self) -> str:
        return self.book.title