from django.conf import settings
from django.db import models

from catalog.models import Book


class Review(models.Model):
    RATING_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )

    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    value = models.IntegerField(choices=RATING_CHOICES, default=5)
    comment = models.TextField(max_length=1024)
    pub_date = models.DateTimeField(auto_now_add=True, verbose_name='Publication date')


    class Meta:
        verbose_name = 'Review'
        verbose_name_plural = 'Reviews'
        ordering = ['-pub_date']

    def __str__(self) -> str:
        return str(self.id)
