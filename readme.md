# Examples

# User 

### List users

```grapql
{
  users {
    id
    username
  }
}
```

### Create user

```grapql
mutation {
  userCreate(
    username: "maria", 
    email:" maria@gmail.com", 
    password:"tecarch123") {
    user {
      id
      username
      email
      dateJoined
    }
  }
}
```


### Login user

```grapql
mutation {
  tokenAuth(username: "maria", password: "tecarch123") {
	token
    payload
    refreshExpiresIn
  }
}
```

## Review

### Create review

```grapql
mutation {
  createReview(input:{book: 1, comment: "Love this book", value: 5}) {
    review {
			pubDate
      comment
      value
      user {
        username
      }
    }
  }
}
```

### Update review

```grapql
mutation {
		updateReview(input:{id: 11, book: 1, comment: "Love book", value: 5}) {
    review {
			pubDate
      comment
      value
      user {
        username
      }
    }
  }
}
```

### Delete review

```grapql
mutation {
  deleteReview(id: 11) {
    ok
  }
}
```
